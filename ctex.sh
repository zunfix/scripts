#!/bin/sh
# A simple script to compile latex documents and clean up files

latexmk -lualatex -synctex=0 -pdf "$1" 
latexmk -c
