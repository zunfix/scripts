#!/bin/sh

pamixer --mute
swaylock -i ~/git/wallpapers/013.jpg --ring-color 202020E0 --text-color FFFFFF --indicator-thickness 5 --inside-color 00000030 --indicator-x-position 150 --indicator-y-position 1270
pulsemixer --unmute
