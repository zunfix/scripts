#!/bin/sh
set -e

base_packages=(
	alacritty
	amf-amdgpu-pro
	bat
	base-devel
	blueman
	bluez
	bluez-utils
	btop
	calcurse
	cups
	cosmic
	discord
	droidcam
	firefox
	gnumeric
	gst-plugin-pipewire
	imv
	lf
	libpulse
	lib32-vulkan-radeon
	linux-headers
	man-db
	mold
	mpv
	neovim
	nextcloud-client
	nmap
	noto-fonts
	ntp
	otf-ipafont
	piavpn-bin
	pipewire
	pipewire-alsa
	pipewire-jack
	pipewire-pulse
	protonmail-bridge
	qbittorrent
	spaceship-prompt
	steam
	tar
	thunderbird
	tmux
	ttf-hack-nerd
	unzip
	v4l2loopback-dc-dkms
	vulkan-radeon
	wireplumber
	xpadneo-dkms
	xone-dlundqvist-dkms-git
	xone-dongle-firmware
	zsh
	zsh-autosuggestions
	zsh-syntax-highlighting
)

# these sed commands were taken from https://giacomo.coletto.io/blog/arch-conf/
sudo sed -i "/etc/pacman.conf" \
    -e "s|^#Color|&\nColor|" \
    -e "s|^#\[multilib\]|&\n[multilib]\nInclude = /etc/pacman.d/mirrorlist|" \
    -e "s|^#ParallelDownloads.*|&\nParallelDownloads = 20|"

sudo sed -i "/etc/makepkg.conf" \
    -e "s|^#BUILDDIR=.*|&\nBUILDDIR=/tmp/makepkg|" \
    -e "s|^PKGEXT.*|PKGEXT='.pkg.tar'|" \
    -e "s|^OPTIONS=.*|#&\nOPTIONS=(docs \!strip \!libtool \!staticlibs emptydirs zipman purge \!debug lto)|" \
    -e "s|-march=.* -mtune=generic|-march=native|" \
    -e "s|^#RUSTFLAGS=.*|&\nRUSTFLAGS=\"-C opt-level=2 -C target-cpu=native\"|" \
    -e "s|^#MAKEFLAGS=.*|&\nMAKEFLAGS=\"-j$(($(nproc --all)-1))\"|"

# auto unlock keyring with login
sudo echo "auth       optional     pam_gnome_keyring.so" >> /etc/pam.d/login
sudo echo "session    optional     pam_gnome_keyring.so auto_start" >> /etc/pam.d/login

# set up aur helper
cd /tmp
git clone "https://aur.archlinux.org/paru.git"
sudo pacman -Syu --noconfirm --needed rustup
rustup default stable
cd paru
makepkg -si --noconfirm
cd ~

# Install microcode
proc_type=$(lscpu)
if grep -E "GenuineIntel" <<< ${proc_type}; then
    echo "Installing Intel microcode"
    sudo pacman -S --noconfirm --needed intel-ucode
elif grep -E "AuthenticAMD" <<< ${proc_type}; then
    echo "Installing AMD microcode"
    sudo pacman -S --noconfirm --needed amd-ucode
fi

paru -Syu ${base_packages[@]} --noconfirm

ln -s /tmp /home/$(whoami)/Downloads
git clone https://gitlab.com/zunfix/dot_files.git ~/git/dot_files
git clone https://gitlab.com/zunfix/scripts.git ~/git/scripts
git clone https://gitlab.com/zunfix/wallpapers.git ~/git/wallpapers

mkdir -p /home/$(whoami)/tmp
mkdir -p /home/$(whoami)/.config
mkdir -p /home/$(whoami)/.local
mkdir -p /home/$(whoami)/.local/bin

ln -s /home/$(whoami)/git/dot_files/* /home/$(whoami)/.config/
ln -s /home/$(whoami)/git/scripts/*.sh /home/$(whoami)/.local/bin/
ln -s /home/$(whoami)/git/dot_files/zsh/zshrc /home/$(whoami)/.zshrc
rm /home/$(whoami)/.config/README.md

# setup tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
/home/$(whoami)/.tmux/plugins/tpm/bin/install_plugins

# Enable services
sudo modprobe btusb
sudo systemctl enable bluetooth
# sudo systemctl enable cups.service
sudo systemctl enable ntpd.service
sudo systemctl enable NetworkManager.service
systemctl enable --user pipewire-pulse.service

git config --global alias.lg "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)' --all"
git config --global commit.verbose true

chsh -s /usr/bin/zsh

cd ~
rm -rf scripts
